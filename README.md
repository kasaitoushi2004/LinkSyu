# LinkSyu

笠井闘志個人ウェブサイトの「リンク集」（ https://kasaitoushi.nagano.jp/ligoj ）で使用しているPHPスクリプト。
見やすさや修正のしやすさのため，複数のechoコマンドに分けています。これをまとめても正常に動作します。